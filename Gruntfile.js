module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    connect: {
      server: {
        options: {
          port: 8000,
          base: 'public/'
        }
      }
    },
    watch:{
      css: {
        files:['app/**/*.scss'],
        tasks:['sass'],
        options: {
          livereload: true,
        }
      },
      html: {
        files:['public/**/*.html'],
        options: {
          livereload: true,
        }
      }
		},
    sass: {                            
      dist: {
        files: {
          'public/main.css': 'app/style/main.scss'
        }
      }
    }  
  });

  // Local server
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');

  // Default task(s).
  grunt.registerTask('default', ['connect', 'watch']);
};